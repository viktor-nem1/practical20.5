// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSpeedMax.h"
#include "SnakeBase.h"

ABonusSpeedMax::ABonusSpeedMax()
{
	PrimaryActorTick.bCanEverTick = true;

	UStaticMeshComponent* MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh'/Engine/ArtTools/RenderToTexture/Meshes/S_1_Unit_Plane.S_1_Unit_Plane'"));
	RootComponent = MeshComponent;
}

void ABonusSpeedMax::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->IncreaseSpeed();
		}

		SetActorHiddenInGame(true);
		SetActorEnableCollision(false);

		GetWorldTimerManager().SetTimer(TimerHandle_SpawnNewBonusSpeedMax, this, &ABonusSpeedMax::SpawnNewBonusSpeedMax, 6.0f, false);
	}
}

void ABonusSpeedMax::SpawnNewBonusSpeedMax()
{
	FVector NewLocation;
	FRandomStream RandomStream;
	RandomStream.GenerateNewSeed();
	NewLocation.X = RandomStream.FRandRange(-620.f, 450.f);
	NewLocation.Y = RandomStream.FRandRange(-530.f, 540.f);
	NewLocation.Z = 10.f;

	ABonusSpeedMax* NewBonusSpeedMax = GetWorld()->SpawnActor<ABonusSpeedMax>(GetClass(), NewLocation, FRotator::ZeroRotator);

	GetWorldTimerManager().ClearTimer(TimerHandle_SpawnNewBonusSpeedMax);
	Destroy();
}