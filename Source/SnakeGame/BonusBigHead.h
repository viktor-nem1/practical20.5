// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "TimerManager.h"
#include "BonusBigHead.generated.h"

UCLASS()
class SNAKEGAME_API ABonusBigHead : public AActor, public IInteractable
{
	GENERATED_BODY()

public:
	ABonusBigHead();

		
		virtual void Interact(AActor* Interactor, bool bIsHead) override;

private:
	void SpawnNewBonusBigHead();

	FTimerHandle TimerHandle_SpawnNewBonusBigHead;
};
