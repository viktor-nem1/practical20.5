// Fill out your copyright notice in the Description page of Project Settings.



#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Wall.h"
#include "TimerManager.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
	// Set this actor to call Tick() every frame
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
	BigHeadDuration = 5.0f; // ��������� ���� ��� ������������ ������ ����������� ������
	BigHeadMultiplier = 3.0f; // ��������� ���� ��� ��������� ���������� ������� ������
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);

	for (ASnakeElementBase* SnakeElement : SnakeElements)
	{
		if (SnakeElement != nullptr)
		{
			SnakeElement->SetActorEnableCollision(true);
		}
	}
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation = SnakeElements.Num() > 0 ? SnakeElements[SnakeElements.Num() - 1]->GetActorLocation() + FVector(ElementSize, 0, 0) : FVector(0, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	Moving = false;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}

	AWall* Wall = Cast<AWall>(Other);
	if (Wall)
	{
		Destroy();
	}
}

void ASnakeBase::IncreaseSpeed()
{
	MovementSpeed /= 3.0f;
	SetActorTickInterval(MovementSpeed);
	GetWorldTimerManager().SetTimer(TimerHandle_ResetSpeed, this, &ASnakeBase::ResetSpeed, 5.0f, false);
}

void ASnakeBase::ResetSpeed()
{
	MovementSpeed *= 3.0f;
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::SetBigHead(bool bHasBigHead)
{
	if (bHasBigHead)
	{
		ElementSize *= BigHeadMultiplier;
		GetWorldTimerManager().SetTimer(TimerHandle_ResetBigHead, this, &ASnakeBase::ResetBigHead, BigHeadDuration, false);
	}
	else
	{
		ResetBigHead();
	}
}

void ASnakeBase::ResetBigHead()
{
	ElementSize /= BigHeadMultiplier;
}