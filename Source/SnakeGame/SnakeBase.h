// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class ABonusFoodCount;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()

public:
	ASnakeBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Snake")
		float SnakeSpeed;

	void IncreaseSpeed();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	UPROPERTY()
		bool Moving;

	UPROPERTY(EditDefaultsOnly)
		float BigHeadDuration; // ������������ ������ ����������� ������

	UPROPERTY(EditDefaultsOnly)
		float BigHeadMultiplier; // ��������� ���������� ������� ������

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
		void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	void SetBonusFoodCount(ABonusFoodCount* BonusCounter);

	void SetBigHead(bool bHasBigHead);

private:
	FTimerHandle TimerHandle_ResetSpeed;
	FTimerHandle TimerHandle_ResetBigHead;
	ABonusFoodCount* BonusFoodCount;

	void ResetSpeed();
	void ResetBigHead();
};