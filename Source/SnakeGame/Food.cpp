// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Math/RandomStream.h"

// Sets default values
AFood::AFood()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AFood::BeginPlay()
{
	Super::BeginPlay();
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Destroy();

			SpawnNewFood();
		}
	}
}

void AFood::SpawnNewFood()
{
	FVector NewLocation;
	FRandomStream RandomStream;
	RandomStream.GenerateNewSeed();
	NewLocation.X = RandomStream.FRandRange(-620.f, 450.f);
	NewLocation.Y = RandomStream.FRandRange(-530.f, 540.f);
	NewLocation.Z = 10.f;

	AFood* NewFood = GetWorld()->SpawnActor<AFood>(GetClass(), NewLocation, FRotator::ZeroRotator);
}

