// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusBigHead.h"
#include "SnakeBase.h"
#include "TimerManager.h"

// Sets default values
ABonusBigHead::ABonusBigHead()
{
	PrimaryActorTick.bCanEverTick = true;

	UStaticMeshComponent* MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh'/Engine/ArtTools/RenderToTexture/Meshes/S_1_Unit_Plane.S_1_Unit_Plane'"));
	RootComponent = MeshComponent;
}

void ABonusBigHead::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetBigHead(true);
		}
		SetActorHiddenInGame(true);
		SetActorEnableCollision(false);

		GetWorldTimerManager().SetTimer(TimerHandle_SpawnNewBonusBigHead, this, &ABonusBigHead::SpawnNewBonusBigHead, 6.0f, false);
	}
}

void ABonusBigHead::SpawnNewBonusBigHead()
{
	FVector NewLocation;
	FRandomStream RandomStream;
	RandomStream.GenerateNewSeed();
	NewLocation.X = RandomStream.FRandRange(-620.f, 450.f);
	NewLocation.Y = RandomStream.FRandRange(-530.f, 540.f);
	NewLocation.Z = 10.f;

	ABonusBigHead* NewBonusBigHead = GetWorld()->SpawnActor<ABonusBigHead>(GetClass(), NewLocation, FRotator::ZeroRotator);

	GetWorldTimerManager().ClearTimer(TimerHandle_SpawnNewBonusBigHead);
	Destroy();
}