// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BonusFoodCount.generated.h"

UCLASS()
class SNAKEGAME_API ABonusFoodCount : public AActor
{
	GENERATED_BODY()

public:
	ABonusFoodCount();

	UPROPERTY(BlueprintReadWrite)
		int32 FoodCount;

protected:
	virtual void BeginPlay() override;

public:
	void IncrementFoodCount();

private:

	void SpawnBonusFood(); 
};
