// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "BonusSpeedMax.generated.h"

UCLASS()
class SNAKEGAME_API ABonusSpeedMax : public AActor, public IInteractable
{
	GENERATED_BODY()

public:
	ABonusSpeedMax();

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

private:
	void SpawnNewBonusSpeedMax();

	FTimerHandle TimerHandle_SpawnNewBonusSpeedMax;
};