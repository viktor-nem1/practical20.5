// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusFoodCount.h"
#include "BonusSpeedMax.h"
#include "Kismet/GameplayStatics.h"
#include "Math/RandomStream.h"
#include "Engine/World.h" 

ABonusFoodCount::ABonusFoodCount()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ABonusFoodCount::BeginPlay()
{
	Super::BeginPlay();
	FoodCount = 0;
}

void ABonusFoodCount::IncrementFoodCount()
{
	FoodCount++;
	if (FoodCount % 3 == 0)
	{
		SpawnBonusFood();
	}
}

void ABonusFoodCount::SpawnBonusFood()
{
	UWorld* World = GetWorld();
	if (World)
	{
		FVector NewLocation;
		FRandomStream RandomStream;
		RandomStream.GenerateNewSeed();
		NewLocation.X = RandomStream.FRandRange(-620.f, 450.f);
		NewLocation.Y = RandomStream.FRandRange(-530.f, 540.f);
		NewLocation.Z = 10.f;

		ABonusSpeedMax* NewBonusSpeedMax = World->SpawnActor<ABonusSpeedMax>(ABonusSpeedMax::StaticClass(), NewLocation, FRotator::ZeroRotator);
		
	}
}

